let dsnv = [];
const DSNV = `DSNV`;

function saveLocalStorage() {
  // Convert JSON
  let myJSON = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, myJSON);
}

let dataJSON = localStorage.getItem(DSNV);
if (dataJSON !== null) {
  let nvArr = JSON.parse(dataJSON);
  for (let i = 0; i < nvArr.length; i++) {
    let item = nvArr[i];
    let nv = new NhanVien(
      item.account,
      item.name,
      item.email,
      item.passWord,
      item.date,
      item.incomeCB,
      item.position,
      item.time
    );
    dsnv.push(nv);
  }
  renderDsnv(dsnv);
}

function addUser() {
  let nv = getInputFromForm();
  // temp +1 nếu các function validate đều trả về giá trị true
  temp = checkValidation(nv);
  if (temp == 8) {
    dsnv.push(nv);
    saveLocalStorage();
    renderDsnv(dsnv);
  }
}

function deleteUser(acc) {
  let index = findIndex(acc, dsnv);
  if (index !== -1) {
    dsnv.splice(index, 1);
    // Re-render
    renderDsnv(dsnv);
  }
  saveLocalStorage();
}

function editUser(acc) {
  let index = findIndex(acc, dsnv);
  if (index == -1) return;
  let editedUser = dsnv[index];
  showOuput(editedUser);
  // document.getElementById("tknv").disabled = true;
  // document.getElementById("btnThemNV").disabled = true;
}

function updateUser() {
  let data = getInputFromForm();
  let isValid = checkWhenUpdateUser(data);
  if (isValid == 7) {
    let index = findIndex(data.account, dsnv);
    if (index == -1) return;
    dsnv[index] = data;
    renderDsnv(dsnv);
    saveLocalStorage();
  }
}

function searchUser() {
  let listTypeNV = [];
  let dataFilter = document.getElementById("searchName").value.toLowerCase();
  dsnv.forEach((nv) => {
    // Gán biến rank = xếp loại nhân viên
    let rank = nv.ranking();
    // Biến temp dùng để kiểm tra trong Xếp loại có giá trị cần tìm hay không
    let temp = rank.indexOf(dataFilter.trim());
    // "Nhân viên xyz" => chữ x là bắt đầu từ vị trí thứ 10
    // => Nếu temp tìm thấy Xếp loại sẽ trả về 10
    if (temp == 10) {
      listTypeNV.push(nv);
    }
  });
  renderDsnv(listTypeNV);
}
